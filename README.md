# Cucumber Tests for Course Information System

### Submitted by: Sunil Prajapati
### ID: 124073

This is rails application with cucumber tests for Project model. 

To run the application, first install the packages using command: 

`bundle install`

Then create the databases and migrate the tables using the command:

`rake db:create`

`rake db:migrate`

To run the cucumber test, please run the command:

`bundle exec cucumber`


The tests are located at features folder
