FactoryBot.define do
    # This will create a Project class
    factory :project do
      name { 'FSAD' }
      url  { 'www.fsad.com' }
    end

    # This will create a Student class
    factory :student do 
        sequence(:name) { |n| "Student #{n}" }
        sequence(:studentid) { |n| "#{n}" }

    end 


  end